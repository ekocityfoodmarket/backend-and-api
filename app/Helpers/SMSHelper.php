<?php
namespace App\Helpers;

use Twilio\Exceptions\ConfigurationException;
use Twilio\Exceptions\TwilioException;
use Twilio\Rest\Client;

class SMS {

    /**
     * @param int $to
     * @param int $code
     * @return bool|string
     */
    public static function sendOTP(int $to, int $code)
    {
        $client = (new SMS)->initialize();
        try {
            $client->messages->create(
                $to,
                [
                    'from' => env('APP_NAME', 'MeatHub'),
                    'body' => "Your OTP is $code",
                ]
            );

        } catch (TwilioException $e) {
            return $e->getMessage();
        }
        return true;
    }

    /**
     * @return string|Client
     */
    private function initialize(){

        $sid    = env('TWILIO_SID', 'AC00c0e276ecad308b1e898529d617f85a');
        $token  = env('TWILIO_TOKEN', '5fff3ee3c7f801dce65cc6e8b9b72a55');
        try {
            $client = new Client($sid, $token);
        } catch (ConfigurationException $e) {
            return  $e->getMessage();
        }

        return $client;
    }
}
