<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class UserLocation extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function location()
    {
        return $this->hasOne(Location::class, 'location_id', 'id');
    }
}
