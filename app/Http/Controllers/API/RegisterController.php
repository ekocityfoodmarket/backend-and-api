<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Controllers\EmailController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use PHPUnit\TextUI\Help;
use Validator;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use SendGrid\Mail\TypeException;
use Illuminate\Support\Carbon;
use App\Helpers\SMS;


use App\User;
use App\Models\UserCode;
use App\Helper;


class RegisterController extends Controller
{
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|int|string
     * @throws \SendGrid\Mail\TypeException
     */
    public function authenticate(Request $request) {
        $phone_number = $request->phone_number;//format the number to remove the first 0, should it be received.
        $defaultRequests = ['register', 'registerWithPhone', 'login', 'resendOTP'];//set the default request types
        // request_type : register, login or resendOTP
        $password = $request->password;
        //......................................................................................................................
        $isEmpty = ['phone number' => $phone_number, 'request type' => $request->request_type];
        if(Helper::checkEmptyFields($isEmpty)){return Helper::jsonResponse(false, Helper::checkEmptyFields($isEmpty));}
        if(!in_array($request->request_type, $defaultRequests)) {return Helper::jsonResponse(false, 'Invalid request type sent!');}

        //switch between the type received
        switch ($request->request_type){
            case 'register':
                //call the register method here
                $full_name = ucwords($request->full_name);
                $email_address = strtolower($request->email);
                $referral_code = $request->referral_code;
                return $this->signUp($full_name, $phone_number, $email_address, $password, $referral_code);
                break;
            case 'registerWithPhone':
                $referral_code = $request->referral_code;
                return $this->phoneSignUp($phone_number, $referral_code);
                break;

            case 'resendOTP':
                return $this->resendOTP($phone_number);
                break;

            case 'login':
                return $this->login($request);
                break;

            default:
                return jsonResponse(false, 'Invalid request type sent!');
        }
    }

    private function phoneSignUp(string $phone, $referralCode = null) {
        if(Helper::validatePhone($phone)) return Helper::validatePhone($phone);

        //format the phone number
        $formattedPhone = '+234'.(int)$phone;
        $Phone = User::checkIfExists('phone_number', $formattedPhone);

        if($Phone && $Phone->is_phone_verified)return Helper::jsonResponse(false, 'An account with this number already exists.');
        //if phone number was not verified
        if($Phone && !$Phone->is_phone_verified){
            //send otp to the phone and email
            $OTP = UserCode::createNewOTP($Phone->id, $formattedPhone, "Do not send otp to mobile");
            SMS::sendOTP($formattedPhone, $OTP);

            return Helper::jsonResponse(true, 'Please check your email for the verification code.', 200, $OTP);
        } else {
            //create the new user and create a new  token
            $User = User::createNewUserWithPhone($formattedPhone);
            $OTP = UserCode::createNewOTP($User->id, $formattedPhone, 'Do not send otp to mobile');

            SMS::sendOTP($formattedPhone, $OTP);
        }

        return Helper::jsonResponse(true, 'Please check your phone for the verification code.', 200, $OTP);
    }

    /**
     * this handles the user enrollment into the platform
     * @param $full_name
     * @param $phone
     * @param $email_address
     * @param $password
     * @param null $referralCode
     * @return \Illuminate\Http\JsonResponse
     * @throws \SendGrid\Mail\TypeException
     */
    private function signUp($full_name, $phone, $email_address, $password, $referralCode = null){
        if(Helper::validatePhone($phone)) return Helper::validatePhone($phone);

        //format the phone number
        $formattedPhone = '+234'.(int)$phone;

        $isEmpty = ['name'=>$full_name, 'email address'=>$email_address, 'password'=>$password];
        if(Helper::checkEmptyFields($isEmpty)){return Helper::jsonResponse(false, Helper::checkEmptyFields($isEmpty));}
        $isValid = Helper::validateNamedString($full_name);
        if(!$isValid)return Helper::jsonResponse(false, 'The name entered is invalid.');
        if(!filter_var($email_address, FILTER_VALIDATE_EMAIL))return Helper::jsonResponse(false, 'Invalid email address.');

        $Phone = User::checkIfExists('phone_number', $formattedPhone);
        if($Phone && $Phone->is_phone_verified)return Helper::jsonResponse(false, 'An account with this number already exists.');
        //if phone number was not verified
        if($Phone && !$Phone->is_phone_verified){
            //send otp to the phone and email
            $OTP = UserCode::createNewOTP($Phone->id, $formattedPhone, "Do not send otp to mobile");
            Helper::emailOTP($Phone, $OTP, 'Verify OTP');

            return Helper::jsonResponse(true, 'Please check your email for the verification code.', 200, $OTP);
        }

        $Email = User::checkIfExists('email', $email_address);
        $User = null;
        if($Email){
            //update the phone number here...
            $User =  User::updatePhoneViaEmail($email_address, $formattedPhone, $full_name, $password);
        } else {
            //create the new user and create a new  token
            $User = User::createNewUser($full_name, $formattedPhone, $email_address, $password);

            $emailToken = Str::random(15);
            // create the user's unique referral code if not exists
            // RiderReferralCode::assignReferralCode($User->id);

            //if the referral code is not null, then add the referral code policy to the member who owns it
            if(!is_null($referralCode) || $referralCode){
                //process the referral logic here...
            }

            //send a verification email to the enrolled user.
            $recipient_name = $User->name;
            $d = ['recipient_name'=>$recipient_name];
            $configData = [
                'sender_email'=> getenv('APP_EMAIL'),
                'sender_name'=> getenv('APP_NAME'),
                'recipient_email'=> $email_address,
                'recipient_name'=> $recipient_name,
                'subject'=> 'Welcome'
            ];

            EmailController::dispatchMail($configData, 'emails.welcome', $d);
        }
        //..................................................................................................................
        $OTP = $this->createNewOTP($User->id, $formattedPhone, 'Do not send otp to mobile');
        Helper::emailOTP($User, $OTP, 'Verify OTP');
        //..................................................................................................................
        return Helper::jsonResponse(true, 'Please check your email for the verification code.', 200, $OTP);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request){
        $phone_number = $request->phone_number;
        $password = $request->password;

        $isEmpty = ['phone' => $phone_number, 'password'=>$password];

        if(Helper::checkEmptyFields($isEmpty))return Helper::jsonResponse(false, Helper::checkEmptyFields($isEmpty));

        if(is_null($phone_number))return Helper::jsonResponse(false, 'Invalid phone number entered.');
        if(strlen($phone_number) > 11 || strlen($phone_number) < 11)return Helper::jsonResponse(false, 'Phone number should contain only 11 digits.');
        $formattedPhone = '+234'.(int)$phone_number;//convert to Nigeria
        $loginData = ['phone_number'=>$formattedPhone, 'password'=>$password];

        //validates the login credentials
        if(!Auth::attempt($loginData))return Helper::jsonResponse(false, 'Invalid login credentials.');
        $user = User::where('phone_number', $formattedPhone)->first();

        //create a new access token when login is successful
        $access_token = Helper::createAccessToken($user);

        /**
         * Do other things to initialize the experience of a user
         */

        $data = ['access_token'=>$access_token, 'user'=>$user];

        //deletes any other access tokens from the database already created before this.
        Helper::deleteEarliestUserAccessToken($user->id);

        return Helper::jsonResponse(true, 'Login successful', 200, $data);
    }

    /**
     * The OTP Verification section
     * @param $phone_number
     * @return \Illuminate\Http\JsonResponse|int|string
     * @throws TypeException
     */
    private function resendOTP($phone_number) {
        $USER_NAME = null;
        $column = null;
        if(is_numeric($phone_number)){
            if(Helper::validatePhone($phone_number)) return Helper::validatePhone($phone_number);
            //format the phone number
            $USER_NAME = '+234'.(int)$phone_number;
            $column = 'phone_number';

        } else {
            $column = 'email';
            $USER_NAME = $phone_number;
        }

        $User = User::where($column, $USER_NAME)->first();

        if(!$User) return Helper::jsonResponse(false, "Invalid credentials entered.");

        $userID = $User->id;
        $OTP = null;
        switch ($column){
            case 'phone_number':
                //send a verification code to the number
                $OTP = UserCode::createNewOTP($userID, $USER_NAME);

                SMS::sendOTP($phone_number, $OTP);
                break;

            case 'email':
                $OTP = UserCode::createNewOTP($userID, $USER_NAME, 'email');
                Helper::emailOTP($User, $OTP, 'Verify OTP');
                break;

            default:
                return Helper::jsonResponse(false, "An error occurred.");
        }

        return Helper::jsonResponse(true, 'OTP sent!', 200, ['otp' => $OTP, 'phone_number' => $phone_number]);
    }

    //Send a change of password request
    public function requestPasswordChange(Request $request){
        $phone = $request->phone_number;
        $isEmpty = ['phone number' => $phone];
        if(Helper::checkEmptyFields($isEmpty)) return Helper::jsonResponse(false, Helper::checkEmptyFields($isEmpty));
        if(Helper::validatePhone($phone)) return Helper::validatePhone($phone);
        $formattedPhone = "+234".(int)$phone;

        $User = User::getUser($formattedPhone);
        if(!$User) return Helper::jsonResponse(false, "Phone number is invalid.");

        $OTP = UserCode::createNewOTP($User->id, $formattedPhone);

        return Helper::jsonResponse(true, "New OTP sent!", 200, $OTP);
    }

    /**
     * validate a Driver's phone number for changing a default password
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updatePassword(Request $request){
        $phone = $request->phone_number;
        $password = $request->password;
        $otp = $request->otp;

        $isEmpty = ['phone_number'=>$phone, 'password'=>$password, 'otp'=>$otp];
        if(Helper::checkEmptyFields($isEmpty)) return Helper::jsonResponse(false, Helper::checkEmptyFields($isEmpty));
        if(Helper::validatePhone($phone)) return Helper::validatePhone($phone);

        $formatted = "+234".(int)$phone;
        $User = User::checkIfExists('phone_number', $formatted);
        if(!$User) return Helper::jsonResponse(false, "Invalid phone number entered.");

        $notIsOTPValid = UserCode::validateOTP($otp, $formatted);
        if($notIsOTPValid) return $notIsOTPValid;

        //update credentials
        User::updateRecord($User->id, ['password'=> $password, 'is_default_pass_changed'=> 'Yes']);
        UserCode::updateCodeRec($User->id);

        return Helper::jsonResponse(true, "Password changed successfully.", 200);
    }

    /**
     * validate a Driver's phone number for changing a default password
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePassword(Request $request){
        $phone = $request->phone_number;
        $password = $request->password;

        $isEmpty = ['phone_number'=>$phone, 'password'=>$password];

        if(Helper::checkEmptyFields($isEmpty)) return Helper::jsonResponse(false, Helper::checkEmptyFields($isEmpty));
        if(Helper::validatePhone($phone)) return Helper::validatePhone($phone);

        $formatted = "+234".(int)$phone;
        $User = User::checkIfExists('phone_number', $formatted);
        if(!$User) return Helper::jsonResponse(false, "Invalid phone number entered.");

        //update credentials
        User::updateRecord($User->id, ['password'=>$password, 'is_default_pass_changed'=>'Yes']);

        return Helper::jsonResponse(true, "Password changed successfully.", 200);
    }

    public function returnUsers(Request $request) {
        return $request->user();
    }

    /**
     * The OTP Verification section
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function verifyOTP(Request $request){
        $otp = $request->otp_code;
        $phone = $request->phone;

        if(!$otp){return Helper::jsonResponse(false, 'The OTP field is required.');}

        $UserCode = UserCode::where('otp', $otp)
            ->latest()
            ->first();

        if(!$UserCode){return Helper::jsonResponse(false, 'Invalid OTP sent!');}
        if((int)$UserCode->is_used){return Helper::jsonResponse(false, 'OTP has already been used.');}

        //check if OTP has expired after 30 seconds
        $updated_at = Carbon::make($UserCode->updated_at);
        if(Helper::validateOTP($updated_at))return Helper::jsonResponse(false, 'OTP already expired');

        //update the user otp , create an access token and get the user data
        $UserCode->is_used = 1;
        $UserCode->save();
        //..................................................................................................................
        $User = User::where('id', $UserCode->user_id)->first();
        if(!$User->is_phone_verified){
            $User->is_phone_verified = 1;
            $User->save();
        }
        //..................................................................................................................
        $AccessData = $this->returnAccessData($User);
        return Helper::jsonResponse(true, 'Success', 200, $AccessData);
    }


    /**
     * create the access login data
     * @param $UserModel
     * @return array
     */
    private function returnAccessData (User $UserModel){
        $AccessToken = $this->createAccessToken($UserModel);
        $UserData = [
            'access_token' => $AccessToken,
            'user' => $UserModel
        ];
        return $UserData;
    }

    public function mereUser()
    {
        $User = User::find(1);
//        return $User;
        return $this->returnAccessData($User);
    }

    /**
     * create a user access token
     * @param User $user
     * @return string
     */
    private function createAccessToken(User $user){
        try {
            return $AccessToken = Helper::createAccessToken($user);
        } catch (\Exception $e) {
            return 'doesnot work';
        }
    }

}
