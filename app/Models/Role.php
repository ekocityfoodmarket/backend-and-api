<?php

namespace App\Models;

use App\User;
use App\Models\UserRole;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public function userrole()
    {
        return $this->belongsTo(UserRole::class, 'role_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
