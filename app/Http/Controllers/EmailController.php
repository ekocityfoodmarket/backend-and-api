<?php

namespace App\Http\Controllers;

use App\Mail\MailNotify;
use App\User;
use App\UserCode;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;
use SendGrid\Mail\Attachment;
use SendGrid\Mail\TypeException;

class EmailController extends Controller{
    /**
     * this is the SendGrid API key
     * @return string
     */
    private static function getKey(){
        return $api_key =  getenv('SENDGRID_API_KEY');
    }

    /**
     * sends a copy of the Plentywaka database file
     */
    public static function sendDatabaseFile(){
        $data = [];
        $config = [
            'sender_email' => 'info@plentywaka.com',
            'sender_name' => 'Plentywaka',
            'subject' => 'Plentywaka Database Backup',
            'recipient_email' => 'plentywaka.dev@gmail.com',
            'recipient_name' => 'Plentywaka Devs'
        ];

        try {
            self::dispatchMail($config, 'emails.db_mail', $data, true);

        } catch (TypeException $e) {}

    }

    /**
     * THIS IS A MULTIPURPOSE EMAIL DISPATCHER
     * @param array $emailConfig
     * @param string $bladeTemplate
     * @param array $bladeData
     * @param bool $withAttachment
     * @return bool
     * @throws \SendGrid\Mail\TypeException
     */
    public static function dispatchMail(array $emailConfig, string $bladeTemplate = '', $bladeData = [], $withAttachment = false){
        $api_key = self::getKey();

        $email = new \SendGrid\Mail\Mail();
        $email->setFrom($emailConfig['sender_email'], $emailConfig['sender_name']);
        $email->setSubject($emailConfig['subject']);
        $email->addTo($emailConfig['recipient_email'], $emailConfig['recipient_name']);

        if(!$withAttachment){
            $view = View::make($bladeTemplate, $bladeData);
            $html = $view->render();//fetch the content of the blade template
            $email->addContent( "text/html", $html);
        }

        //if this is set to true
        if($withAttachment){
            $email->addContent( "text/plain", 'Latest Plentywaka DB Backup');
            $path =  storage_path('db_backup/');
            $file_name = 'plentywaka_dump.sql';
            $attachment = $path.$file_name;
            $content    = file_get_contents($attachment);
            //$content    = chunk_split(base64_encode($content));

            $attachment = new Attachment();
            $attachment->setContent($content);
            $attachment->setType("application/sql");
            $attachment->setFilename(env('APP_ENV') . '_' .date('Y-m-d') . "_$file_name");
            $attachment->setDisposition("attachment");
            $email->addAttachment($attachment);
        }

        $sendgrid = new \SendGrid($api_key);

        try {
            $response = $sendgrid->send($email);

        } catch (TypeException $e) {
            return false;
        }
    }

}
